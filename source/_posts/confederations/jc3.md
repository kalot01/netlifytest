---
type: confederation
title: JC3 (Canada)
logo: /assets/logos/jc3.png
no_of_JEs: 6
no_of_JEurs: 100
no_of_projects: 0
turnover:
conversion_rate:
skip: false
short_url: jc3.ca
full_url: https://jc3.ca/
facebook: https://www.facebook.com/jc3.ca/
instagram:
twitter:
---
