---
type: confederation
title: Brasil Júnior
logo: /assets/logos/brasil-junior.png
no_of_JEs: 805
no_of_JEurs: 20000
no_of_projects: 18302
turnover: 29400005
conversion_rate: 0.2576
skip: false
full_url: 'https://brasiljunior.org.br/'
short_url: brasiljunior.org.br
facebook: 'https://www.facebook.com/brasiljunior/'
instagram: 'https://www.instagram.com/bjnoinsta/'
---
